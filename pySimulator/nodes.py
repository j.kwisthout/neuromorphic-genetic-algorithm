import numpy as np

"""
neurons
"""


class AbstractNeuron():
    """ Abstract class for a neuron

    Attributes
    ----------
    I : float (Default: 0)
        Current membrane voltage of the neuron
    out : float (Default: 0)
        Current neuron output
    """
    I = 0
    out = 0
    V = 0

    def __init__(self, amplitude=1, name=''):
        self.amplitude = amplitude
        self.name = name

    def update_rng(self, rng):
        if hasattr(self, 'rng'):
            self.rng = rng


class LIF(AbstractNeuron):
    """Leaky integrate-and-fire based on the Sandia model

    Parameters
    ----------
    m : float (Default: 0.)
        Leakage constant
    V_init : float (Default: 0.)
        Initial membrane voltage
    V_reset : float (Default: 0.)
        Reset voltage when the neuron has spiked
    thr : float (Default: 0.99)
        Spiking threshold
    amplitude : float (Default: 1.)
        Amplitude of the output when the neuron spikes
    I_e : float (Default: 0.)
        Constant input current
    refrac_time : int (Default: 0)
        Refractory period in time steps during which the neuron remains
        at V_reset
    noise : float (Default: 0.)
        Upper limit of the uniform distribution that is sampled from
        to add noise to the membrane voltage at each step
    """

    def __init__(self, m=0., V_init=0., V_reset=0., V_min=0., thr=0.99, amplitude=1., I_e=0.,
                 refrac_time=0, noise=0., name=''):
        AbstractNeuron.__init__(self, amplitude, name)
        self.m = m
        self.V = V_init
        self.V_reset = V_reset
        self.V_min = V_min
        self.thr = thr
        self.I_e = I_e
        self.refrac_time = refrac_time
        self.r_count = 0
        self.noise = noise
        self.name = name

    def step(self):
        # Check if in refractory state
        if self.r_count > 0:
            self.I = self.I_e
            self.out = 0
            self.r_count -= 1
        # Update state if not in refractory state
        else:
            self.V = self.V * self.m + self.I  # update V
            if self.noise > 0:
                self.V += np.random.uniform(low=0.0, high=self.noise)  # add noise
            self.V = max(self.V_min, self.V)
            self.I = self.I_e  # reset I with I_e
            if self.V > self.thr:  # check for spike
                self.V = self.V_reset
                self.out = self.amplitude
                self.r_count = self.refrac_time
            else:
                self.out = 0


"""
Generators
"""


class InputTrain(AbstractNeuron):
    """Generator that outputs a given train of events

    Parameters
    ----------
    train : array_like
        Output train
    loop : boolean
        If true, the train will be looped. Otherwise, the output is 0 at the end of the train.
    """

    def __init__(self, train, loop, name=''):
        AbstractNeuron.__init__(self, 1)
        self.train = train
        self.loop = loop
        self.size = len(train)
        self.index = 0
        self.name = name

    def step(self):
        if self.index >= self.size:
            if self.loop:
                self.index = 0
                self.V = self.train[self.index]
            else:
                self.V = 0

        else:
            self.V = self.train[self.index]

        self.out = self.V
        self.index += 1


class PoissonGenerator(AbstractNeuron):
    """Generator that fires with Poisson statistics, i.e. exponentially
    distributed interspike intervals.

    Parameters
    ----------
    I_e : float (Default: 0)
        Constant input current
    amplitude : float (Default: 1)
        Amplitude of the output
    rng : np.random.RandomState
        Random generator
    """

    def __init__(self, I_e=0, amplitude=1, rng=None):
        AbstractNeuron.__init__(self, amplitude)
        self.I_e = I_e
        self.rng = rng if rng != None else np.random.RandomState()

    def step(self):
        self.V = 0
        if self.I > 0:
            while (-np.log(1 - self.rng.rand()) / self.I) < 1:
                self.V += 1

        self.out = self.V * self.amplitude
        self.I = self.I_e


class RandomSpiker(AbstractNeuron):
    """Generator that fires with a given probability at each time step

    Parameters
    ----------
    I_e : float (Default: 0)
        Constant input current
    amplitude : float (Default: 1)
        Amplitude of the output
    rng : np.random.RandomState
        Random generator
    """

    def __init__(self, p, amplitude=1, rng=None):
        AbstractNeuron.__init__(self, amplitude)
        self.p = p
        self.rng = rng if rng != None else np.random.RandomState()

    def step(self):
        self.V = 0
        if self.rng.rand() < self.p:
            self.V = self.amplitude

        self.out = self.V
