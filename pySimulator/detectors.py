import numpy as np
import matplotlib.pyplot as plt


class Raster():
    def __init__(self, targets):
        self.targets = targets

    def init(self, steps):
        self.spikes = np.zeros((steps, len(self.targets)), dtype=bool)
        self.index = 0

    def step(self):
        self.spikes[self.index, :] = [target.out > 0 for target in self.targets]
        self.index += 1

    def plot(self):
        plt.matshow(self.spikes.T, cmap='gray', fignum=1)
        plt.ylabel("Targets")
        plt.xlabel("Step")
        plt.locator_params(axis='y', nbins=len(self.targets))
        ax = plt.gca()
        ax.set_yticklabels([''] + [i.name for i in self.targets])
        return ax


class Multimeter():
    def __init__(self, targets):
        self.targets = targets

    def init(self, steps):
        self.V = np.zeros((steps, len(self.targets)))
        self.index = 0

    def step(self):
        self.V[self.index, :] = [target.V for target in self.targets]
        self.index += 1

    def plot(self):
        for i in range(len(self.targets)):
            plt.subplot(len(self.targets), 1, 1 + i)
            plt.plot(self.V[:, i])
            plt.ylabel("Voltage " + self.targets[i].name)
            plt.hlines(self.targets[i].thr, xmin=0, xmax=len(self.V[:, i]) - 1, linestyles='--')
        # plt.xticks(range(duration))
        plt.xlabel("Step")
        return plt.gca()
